# The pythondev slack community website

**A flask website built for the pythondev community of slack.**

[![build status](https://gitlab.com/PythonDevCommunity/pythondev-site/badges/master/build.svg)](https://gitlab.com/PythonDevCommunity/pythondev-site/commits/master)

## Join Us

Everyone is welcome !

Request an invitation at: http://pythondevelopers.herokuapp.com/

## Check out the website

The website is live at: http://pythondev-site.herokuapp.com

## Help Us

1. Join us on the #community-projects channel on slack.
2. Read CONTRIBUTING.md
3. Look at the open issues.
4. Select an issues with an appropriate tag for your level of knowledge.
5. Submit a pull request.
6. ...
7. Profit.
