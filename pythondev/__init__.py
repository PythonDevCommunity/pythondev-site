from flask import Flask

from appconfig import config_dict

from logging.handlers import RotatingFileHandler


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config_dict[config_name])
    app.config.from_pyfile('config.cfg', silent=True)
    config_dict[config_name].init_app(app)

    handler = RotatingFileHandler(app.config['LOGGING_LOCATION'],
                                  maxBytes=100000, backupCount=1)
    handler.setLevel(app.config['LOGGING_LEVEL'])
    app.logger.addHandler(handler)

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    return app
