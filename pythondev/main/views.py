# -*- coding: utf-8 -*-
"""User views."""
from flask import Blueprint, render_template

blueprint = Blueprint('home', __name__, url_prefix='', static_folder='../static')


@blueprint.route('/')
@blueprint.route('/index')
def index():
    return render_template('main/index.html')


@blueprint.route('/projects')
def projects():
    return render_template('main/projects.html')
